"""
Imports quoter allowing users to use the extractQuotes function.

To run the program, run:

    python main.py "Text to extract quotes from." 1

"""

import quoter
import argparse

def main():
    """ Main function.
    """
    # Create argument parser
    parser = argparse.ArgumentParser(description="Extract quotes from text.")
    # Add arguments
    parser.add_argument("text", help="Text to extract quotes from.")
    parser.add_argument("count", help="Number of quotes to return.")
    # Parse arguments
    args = parser.parse_args()
    # Extract quotes
    quotes = quoter.extractQuotes(args.text, int(args.count))
    # Print quotes
    for quote in quotes:
        print(quote)

if __name__ == "__main__":
    main()
