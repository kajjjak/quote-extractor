#!/usr/bin/env python
"""
Setup script for the package.

To install the package, run:

    python setup.py install

To create a source distribution, run:

    python setup.py sdist

To create a binary distribution, run:

    python setup.py bdist
    
To create a binary distribution for Windows, run:

    python setup.py bdist_wininst

To create a binary distribution for Mac OS X, run:

    python setup.py bdist_dmg

To create a binary distribution for Linux, run:

    python setup.py bdist_rpm

"""

from setuptools import setup, find_packages

setup(
    name='quoter',
    version='0.1.0',
    description='A library for finding quotes in a text.',
    author='Kjartan Akil Jónsson',

    packages=find_packages(),
    install_requires=[
        
    ],
    extras_require={
        'dev': [
            'pytest',
            'pytest-cov',
            'pytest-mock',
            'pytest-xdist',
            'pytest-watch',
            'tox',
            'flake8',
            'flake8-import-order',
            'flake8-quotes',
            'flake8-builtins',
            'flake8-comprehensions'
        ],
    },
    entry_points={
        'console_scripts': [
            'quoter = main:main',
        ],
    },
)

