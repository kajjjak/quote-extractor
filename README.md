# quote-extractor

## Name
Quote Extractor

## Description
Extracts quotes from text where the quotes extracted retains its meaning without. Essentially choosing the sentances that are most meaningfull within the specified text.

## Installation
Requires python to run.
### Docker

To use with docker you can:

```
docker build -t quoteextractor .
```

And then you can run the Docker container with the following command:

```
docker run --rm quoteextractor -h
```

For example 
```
docker run --rm quoteextractor "Text argument one. This is the second text." 1 
```

## Usage
This is the usage

```
usage: main.py [-h] text count

Extract quotes from text.

positional arguments:
  text        Text to extract quotes from.
  count       Number of quotes to return.

optional arguments:
  -h, --help  show this help message and exit
```

Given by `main.py -h` or `docker run --rm quoteextractor -h`


