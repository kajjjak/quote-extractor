"""
Tests for quoter.py

To run the tests, run:

    python -m unittest discover

"""

import unittest
import quoter

class TestQuoter(unittest.TestCase):

    def test_extractQuotes(self):
        """ Test extractQuotes() function.
        """
        text = """
        Quoter is a library for finding quotes in a text.
        Returning sentances ordered by relevance to the text.
        
        It uses language models teqchniques to find the quotes.
        """
        quotes = quoter.extractQuotes(text, 1)
        self.assertEqual(quotes[0], "Quoter is a library for finding quotes in a text.")