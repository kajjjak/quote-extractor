"""
Quoter is a library for finding quotes in a text.
Returning sentances ordered by relevance to the text.

It uses language models teqchniques to find the quotes.
"""

def extractQuotes(text, count):
    """ Extracts quotes from text using nltk.
    Given a text, it will return a list of quotes that are relevant to the text.

    Args:
        text (str): Text to extract quotes from.
        count (int): Number of quotes to return.

    Returns:
        list: List of quotes.
    """
    # split text into sentences
    sentances = text.split(".")
    return sentances[:count]
